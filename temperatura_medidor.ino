#include <LiquidCrystal.h>
// constantes definidas para LCD
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
// Funcion para leer el dato analogico y convertirlo a digital, grados centigrados
float centi()
{
  int dato;
  float c;
  dato=analogRead(A0); // asifno a dato la lectura analogica
  
  c = (500.0 * dato)/1023; // convierto el dato analogico a digital
/*El sensor de temperatura LM35 responde a variaciones de 10 mV por cada grado centígrado. 
 * Si el sensor detecta 1 grado centígrado a la salida del sensor obtendríamos 10 mV. Ejemplo: 26,4ºC = 264 mV = 0.264 V.
Tenemos que el convertidor de analógico a digital es de 10 bits de resolución,
los valores variarán entre 0 y 1023, entonces Vout= (5V*Dato)/1023 siendo  
( 0 < Dato < 1023 ) y para ajustar la escala a grados centígrados: Vout = ((5V*Dato)*100)/1023*/
  return (c);
}
//funcion de grados centigrados a kelvin
float kelvin(float cent)
{
  float k;
  k=cent+273.15;
  return(k);  
}
//funcion de grados centrigrados a fahrein
float fahren(float cent)
{
  float f;
  f=cent*1.8+32;
  return (f);
}
//Hasta aquí hemos declarado las funciones de conversión del dato analógico de entrada del sensor en grados celsius en sus respectivas equivalencias.

/*float led(float num)
{
 // float num;
  if(num > 20.00)
  {
    digitalWrite(13, HIGH);
    delay(1000);
  }
  return(num);
}*/

void setup() 
{
   pinMode(13, OUTPUT);
   
   Serial.begin(9600); // para imprimir en consola serial
   // Definimos la LCD con dimension 2x16 y definimos los caracteres que deben salir en las filas
  lcd.begin(16,2);
  lcd.print("C=      K=");
  lcd.setCursor(0,1);
  lcd.print("Temperatura HAAS");
}
//Hasta aquí hemos definido qué queremos que salga impreso por la pantalla y el tamaño de ésta.


void loop() 
{ 
  float Centigrados = centi(); // llamados de las funciones y su parametro si es requerido
  float Fahrenheit = fahren (Centigrados);
  float Kelvin = kelvin (Centigrados);
  //led(Centigrados);
 
  //imprimimos por consola
  Serial.print("Centigrados: ");
  Serial.println(Centigrados);
  
  //posicion de salida del valor de la variable, posiciones desde 0 hasta 16 horizontal y desde 0 a 1 vertical
  lcd.setCursor(2,0);
  lcd.print(Centigrados);
  
  lcd.setCursor(10,0);// en la posicion de impresion para que salga despues de la K impresa arriba
  lcd.print(Kelvin);
  
  //Por último, hemos usado la parte activa del programa o bucle para que constantemente recalcule los datos. 
  
  //creamos alerta con led
  if(Centigrados > 20.00)
  {
    digitalWrite(13, HIGH);    
  }
  delay(1000);
  
}
